import Quiz from "./components/Dashboard";
import Home from "./pages/FrontPag";
import './App.css';
import { Route, Routes } from "react-router-dom";

function App() {
  return (
    <>
      <Routes>
      <Route path="/" element={<Home />} />
        <Route path="/quiz" element={<Quiz />} />
      </Routes>
    </>
  );
}

export default App;
