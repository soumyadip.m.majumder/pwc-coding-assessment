import { createTheme } from "@mui/material";

export const theme = createTheme({
  palette: {
    success: {
      main: "#175d2d",
    },
    warning:{
        main:"#ffc83d",
    }
  },
  typography: {
    button: {
      textTransform: "none",
    },
  },
});
