import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import { questions } from "../data/questions";
import { Button, Toolbar } from "@mui/material";
import StarBorderOutlinedIcon from '@mui/icons-material/StarBorderOutlined';

export default function mcqPage() {
  return (
    <>
      {questions.map((ques) => (
        <Card sx={{ minWidth: 275 }} key={ques.key}>
          <CardContent>
            <Toolbar>
            <Typography
              sx={{ fontSize: 14 }}
              color="text.secondary"
              gutterBottom
            >
              Question No . {ques.id}
            </Typography>
            <Button varient="outlined" sx={{ml:"auto" }} color="warning"> <StarBorderOutlinedIcon  sx={{mr:"2px" }}/>Mark for review</Button>
            </Toolbar>
            <Typography variant="h6" component="div">
              {ques.question}
            </Typography>
            <FormControl>
              <RadioGroup
                aria-labelledby="demo-radio-buttons-group-label"
                defaultValue=" "
                name="radio-buttons-group"
              >
                <FormControlLabel
                  value={ques.answers[0]}
                  control={<Radio />}
                  label={ques.answers[0]}
                />
                <FormControlLabel
                  value={ques.answers[1]}
                  control={<Radio />}
                  label={ques.answers[1]}
                />
                <FormControlLabel
                  value={ques.answers[2]}
                  control={<Radio />}
                  label={ques.answers[2]}
                />
                <FormControlLabel
                  value={ques.answers[3]}
                  control={<Radio />}
                  label={ques.answers[3]}
                />
              </RadioGroup>
            </FormControl>
          </CardContent>
          {/* <CardActions>
            <Button size="small"></Button>
          </CardActions> */}
        </Card>
      ))}

      <Button variant="contained" color="success" sx={{ mt: "1em" }}>
        Submit and Next
      </Button>
    </>
  );
}
