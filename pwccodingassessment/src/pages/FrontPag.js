import React, { useState } from "react";
import "./frontPage.css";
import pwclogo from '../assests/pwclogo.png';

const FrontPag = () => {
  const [selectedLanguage, setSelectedLanguage] = useState("");
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [contact, setContact] = useState("");

  const [emailError, setEmailError] = useState("");
  const [firstNameError, setFirstNameError] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [contactError, setContactError] = useState("");

  const handleLanguageChange = (event) => {
    setSelectedLanguage(event.target.value);
  };

  const handleProceed = () => {
    // Validate form fields
    let a = document.getElementById("firstname").value;
    console.log(a);
    let b = document.getElementById("lastname").value;
    console.log(b);
    let c = document.getElementById("email").value;
    console.log(c);
    let d = document.getElementById("contact").value;
    console.log(d);
    let e = document.getElementById("preflang").value;
    console.log(e);

    if (selectedLanguage === "") {
      document.getElementById("error").innerHTML =
        "Please select your preferred language";
    } else {
      document.getElementById("error").innerHTML = "";
    }

    if (!email) {
      setEmailError("Please enter your email");
    } else if (!email.includes("@")) {
      setEmailError("Email must contain '@'");
    } else {
      setEmailError(""); // Clear the error message
    }

    if (!firstName) {
      setFirstNameError("Please enter your first name");
    } else {
      setFirstNameError("");
    }

    if (!lastName) {
      setLastNameError("Please enter your last name");
    } else {
      setLastNameError("");
    }

    if (!contact) {
      setContactError("Please enter your contact information");
    } else if (!/^\d{10}$/.test(contact)) {
      setContactError("Contact must be 10 digits");
    } else {
      setContactError("");
    }
  };

  return (
    <div className="container">
      <div className="left-column">
        <img src={pwclogo} alt="Logo" className="logo" />
        <h2 className="welcome-text">Welcome to</h2>
        <h1 className="welcometext">PWC Coding Assessment</h1>
      </div>  

      <div className="right-column">
        <div className="content"></div>
        <div className="form-container">
          <div className="form-content">
            <div className="input-container">
              <label>Email</label>
              <input
                type="text"
                id="email"
                placeholder="Please enter your email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <div className="error">{emailError}</div>{" "}
              {/* Display error message */}
            </div>

            <div className="input-container">
              <label>First Name</label>
              <input
                type="text"
                id="firstname"
                placeholder="Please enter your first name"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
              />
              <div className="error">{firstNameError}</div>
            </div>
            <div className="input-container">
              <label>Last Name</label>
              <input
                type="text"
                id="lastname"
                placeholder="Please enter your last name"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
              />
              <div className="error">{lastNameError}</div>
            </div>
            <div className="input-container">
              <label>Contact</label>
              <input
                type="text"
                id="contact"
                placeholder="Please enter your contact information"
                value={contact}
                onChange={(e) => setContact(e.target.value)}
              />
              <div className="error">{contactError}</div>
            </div>
            <div className="input-container">
              <label>Select Preferred Language</label>
              <select
                id="preflang"
                value={selectedLanguage}
                onChange={handleLanguageChange}
              >
                <option value="">Select...</option>
                <option value="python">Python</option>
                <option value="c++">C++</option>
                <option value="java">Java</option>
                <option value="javascript">JavaScript</option>
                <option value="c#">C#</option>
              </select>
            </div>
            <p className="error" id="error"></p>
            <button onClick={handleProceed}>Proceed</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FrontPag;
