export const questions = [
  {
    key:"1",
    id: "1",
    question: "React is a ____",
    answers: ["Web development Framework", "JavaScript Library", "jQuery", "Web Server"],
    correctAnswer: " JavaScript Library",
  },
  {
    key:"2",
    id: "2",
    question: "Which ReactJS function renders HTML to the web page?",
    answers: ["render()", "ReactDOM.render()", "renders()", "ReactDOM.renders()",],
    correctAnswer: "ReactDOM.render()",
  },
  {
    key:"3",
    id: "3",
    question: "JSX stands for ___________",
    answers: [
      "JSON",
      "JavaScript XML",
      "JSON XML",
      "JavaScript and AngularJS",
    ],
    correctAnswer: "JavaScript XML",
  },
  {
    key:"4",
    id: "4",
    question: "What are Props?",
    answers: ["Props are arguments passed into React components", "Props are functions in the ReactJS", "Props are used to returns multiple values from the function", "All of the above"],
    correctAnswer: "Props are arguments passed into React components",
  },
  {
    key:"5",
    id: "5",
    question: "Which ReactJS command is used to create a new application?",
    answers: [
      "create-react-app",
      "new-react-app",
      "create-new-reactapp",
      "react-app",
    ],
    correctAnswer: "create-react-app",
  },
];
